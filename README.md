# VQA

Results
-------

1. Stacked Attention with Minet encoder -> 48.68% val accuracy.
2. Stacked Attention with ResNet50 encoder -> 49.75% val accuracy.
3. Stacked Attention with VGG19 encoder -> 49.32% val accuracy (seems the most stable during training).
4. Stacked Attention with InceptionV3 encoder -> 47.86% accuracy (299x299 input).
