import arch
import csv
import keras
import numpy as np
import os
import random
import sys
import tqdm

from concurrent.futures.thread import ThreadPoolExecutor
from keras.callbacks import Callback
from keras.preprocessing.sequence import pad_sequences
from mscoco_v2.config import Config
from mscoco_v2.dataset import Dataset
from settings import QUESTION_LEN, BATCH_SIZE
from tokenizers.spacy_tokenizer import SpaCyTokenizer
from utils import load_GloVe


def _join_dicts(dict1, dict2):
    assert(isinstance(dict1, dict))
    assert(isinstance(dict2, dict))
    res = {}
    for key in dict1:
        res[key] = dict1[key]
    for key in dict2:
        res[key] = dict2[key]
    return res


def _read_features(path):
    assert(isinstance(path, str))
    return np.load(path)


# Returns dict: (question_id, image_id) -> (question, answer, features_path).
def _join_with_images(data, images_dir, path_prefix, sample=None):
    assert(isinstance(data, dict))
    assert(isinstance(images_dir, str))

    if sample is not None:
        assert(isinstance(sample, int))
        keys = random.sample(list(data.keys()), sample)
        subset = {}
        for key in keys:
            subset[key] = data[key]
        data = subset

    res = {}
    # Ex: COCO_val2014_000000000042.npy
    for key, value in tqdm.tqdm(data.items(), desc="Reading image features"):
        # (question_id, image_id) -> (question, answer).
        image_id = str(key[1]).rjust(12, "0")
        features = os.path.join(images_dir, path_prefix + str(image_id) + ".npy")  # noqa: E501
        assert(isinstance(features, str))
        res[key] = (value[0], value[1], features)
    return res


def _extract_sentences(data):
    assert(isinstance(data, dict))
    sentences = set()
    for _, value in data.items():
        assert(len(value) == 3)
        assert(isinstance(value[0], str))
        sentences.add(value[0])
    return sentences


def _preprocess(data, tokenizer):
    assert(isinstance(data, dict))
    answer_index = Config().get_answers_index()

    labels = []
    questions = []
    features = []
    # (question_id, image_id) -> (question, answer, features_path).
    for key, value in data.items():
        assert(len(key) == 2)
        assert(len(value) == 3)
        question = tokenizer.texts_to_sequences([value[0]])
        questions.append(pad_sequences(question, maxlen=QUESTION_LEN)[0])
        features.append(value[2])
        labels.append(answer_index[value[1]])
    assert(len(labels) == len(questions))
    assert(len(labels) == len(features))

    return list(zip(questions, features, labels))


def build_question_embeddings(word_index):
    glove = load_GloVe()
    num_words = len(word_index) + 1
    emb_matrix = np.zeros((num_words, 300))
    known_emb = 0
    for word, i in word_index.items():
        if word in glove:
            emb_matrix[i] = glove[word]
            known_emb += 1
        elif word.lower() in glove:
            emb_matrix[i] = glove[word.lower()]
            known_emb += 1
        elif word.upper() in glove:
            emb_matrix[i] = glove[word.upper()]
            known_emb += 1
    print("Known embeddings: {0:.3f}%".format(100.0 * known_emb / num_words))
    return emb_matrix


# Saves the main model (e.g. the one without embeddings).
class SaveMainModel(Callback):
    def __init__(self, main_model, directory):
        self.main_model = main_model
        self.directory = directory
        super(SaveMainModel, self).__init__()

    def on_epoch_end(self, epoch, logs={}):
        # logs contains {'val_loss', 'val_acc', 'loss', 'acc'}
        val_loss = logs.get('val_loss', 0)
        val_acc = logs.get('val_acc', 0)
        filename = "model-epoch{}-{}-{}.h5".format(
                str(epoch + 1).zfill(4),
                float("{0:.5f}".format(val_loss)),
                float("{0:.5f}".format(val_acc))
        )
        filename = os.path.join(self.directory, filename)
        try:
            self.main_model.save_weights(filename)
        except Exception as ex:
            print("SaveMainModel cannot save model {} - Exception: {}".
                  format(filename, str(ex)))


def generate_batches(data, batch_size):
    assert(isinstance(data, list))
    assert(len(data) >= 1)

    random.shuffle(data)
    chunks = []
    for i in range(0, len(data), batch_size):
        chunks.append(data[i: i + batch_size])
    while len(chunks[-1]) < batch_size:
        chunks[-1].append(random.choice(data))
    for chunk in chunks:
        assert(len(chunk) == batch_size)
    del data
    num_answers = len(Config().get_answers_index())

    def inner_generator():
        def infinite_chunks(data):
            idx = 0
            while True:
                chunk = data[idx]
                idx = (idx + 1) % len(data)
                yield chunk

        executor = ThreadPoolExecutor(max_workers=8)
        # (question, features, label)
        for chunk in infinite_chunks(chunks):
            features_rcnn = np.zeros((batch_size, 100, 2048), dtype=np.float32)
            features = np.zeros((batch_size, 7, 7, 512), dtype=np.float32)
            labels = np.zeros((batch_size, num_answers), dtype=np.int32)
            questions = np.zeros((batch_size, QUESTION_LEN), dtype=np.int32)

            img_ids = [os.path.basename(x[1]).split('_')[-1] for x in chunk]
            img_ids = [int(x[:-4]) for x in img_ids]

            # Read bottom-up attention features.
            features_rcnn_gen = executor.map(
                            lambda img_id: np.load(os.path.join(
                                            "/", "opt", "data", "VQA",
                                            "bottom_up_attention", "trainval",
                                            "data", str(img_id) + ".npy")),
                            img_ids
            )
            loaded_features = [x for x in features_rcnn_gen]

            f_generator = executor.map(
                            lambda path: np.load(path),
                            [x[1] for x in chunk]
            )
            loaded_images = []
            for image in f_generator:
                loaded_images.append(image)

            assert(len(chunk) == batch_size)
            assert(len(loaded_images) == batch_size)
            for i in range(0, batch_size):
                questions[i] = chunk[i][0]
                features[i] = loaded_images[i]
                features_rcnn[i, :loaded_features[i].shape[0], :] = loaded_features[i]  # noqa: E501
                labels[i][chunk[i][2]] = 1

            yield ({
                    "question_input": questions,
                    "image_input": features,
                    "features_input": features_rcnn
            }, labels)

    return inner_generator()


def main():
    train_data = Dataset(
            "/opt/data/VQA/v2_OpenEnded_mscoco_train2014_questions.json",
            "/opt/data/VQA/v2_mscoco_train2014_annotations.json"
    )
    val_data = Dataset(
            "/opt/data/VQA/v2_OpenEnded_mscoco_val2014_questions.json",
            "/opt/data/VQA/v2_mscoco_val2014_annotations.json"
    )
    print(train_data)
    print(val_data)

    train_data = train_data.get_entries()
    val_data = val_data.get_entries()

    # The validation data is selected from the last samples in the x and y data
    # provided, before shuffling.
    train_data = _join_with_images(
                train_data,
                "/opt/data/VQA/vgg19/train2014",
                "COCO_train2014_"
    )
    val_data = _join_with_images(
                val_data,
                "/opt/data/VQA/vgg19/val2014",
                "COCO_val2014_"
    )
    # assert(len(train_data) + len(val_data) == len(data))

    tokenizer = SpaCyTokenizer()
    all_sentences = list(set(
                list(_extract_sentences(train_data)) +
                list(_extract_sentences(val_data))
    ))
    tokenizer.fit_on_texts(all_sentences)

    train_data = _preprocess(train_data, tokenizer)
    val_data = _preprocess(val_data, tokenizer)
    word_index = tokenizer.word_index()
    print('Found %s unique tokens.' % len(word_index))
    full_model, main_model = arch.model_from_features(
                    build_question_embeddings(word_index)
    )

    optimizer = keras.optimizers.Adam(lr=0.001, clipnorm=1.0)
    full_model.compile(loss=keras.losses.categorical_crossentropy,
                       optimizer=optimizer,
                       metrics=['accuracy'])
    full_model.summary()
    main_model.summary()
    print("", flush=True)

    save_model = SaveMainModel(main_model, "trained_models")
    train_generator = generate_batches(train_data, BATCH_SIZE)
    val_generator = generate_batches(val_data, BATCH_SIZE)
    full_model.fit_generator(
            train_generator,
            steps_per_epoch=int(len(train_data) / BATCH_SIZE) + 1,
            validation_data=val_generator,
            validation_steps=int(len(val_data) / BATCH_SIZE) + 1,
            epochs=200,
            verbose=2,
            callbacks=[save_model],
            workers=1,  # Generators are not thread safe.
            use_multiprocessing=False,
            max_queue_size=15
    )


if __name__ == "__main__":
    csv.field_size_limit(sys.maxsize)
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    main()
