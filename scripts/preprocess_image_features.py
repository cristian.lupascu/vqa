import numpy as np
import os

from keras.applications import vgg19
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array


def get_model():
    return vgg19.VGG19(include_top=False, weights='imagenet',
                       input_shape=(224, 224, 3), pooling='none')


def read_image(image_path):
    return load_img(image_path, target_size=(224, 224))


def main(input_dir, output_dir):
    def chunks(array, chunk_size):
        for i in range(0, len(array), chunk_size):
            yield array[i:i + chunk_size]

    # Build the model.
    model = get_model()
    model.summary()

    # Read and preprocess. There is not enough RAM to hold all
    # the images, therefore process in chunks.
    files = os.listdir(input_dir)
    files = [x for x in files if os.path.isfile(os.path.join(input_dir, x))]
    for i, path in enumerate(files):
        filename, file_extension = os.path.splitext(path)
        assert(file_extension in [".jpg"])
        files[i] = (filename, os.path.join(input_dir, path))

    for chunk in chunks(files, 2048):
        data = map(lambda x: (x[0], read_image(x[1])), chunk)
        data = map(lambda x: (x[0], img_to_array(x[1])), data)
        data = list(data)
        image_ids = [x[0] for x in data]
        images = [x[1] for x in data]
        assert(len(image_ids) == len(images))
        images = vgg19.preprocess_input(np.array(images).copy())
        assert(images.shape == (len(image_ids), 224, 224, 3))

        # Predict.
        y = model.predict(images, batch_size=128, verbose=1)
        assert(y.shape[0] == len(image_ids))
        for i in range(0, len(image_ids)):
            filename = image_ids[i] + ".npy"
            features = y[i]
            np.save(os.path.join(output_dir, filename), features)


if __name__ == "__main__":
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = "1"

    main(
            "/opt/data/VQA/train2014",
            "/opt/data/VQA/vgg19/train2014"
    )
