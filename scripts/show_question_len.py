import os
import spacy
import sys
import tqdm


def to_questions(data):
    assert(isinstance(data, dict))
    questions = []
    for _, value in data.items():
        questions.append(value[0])
    return questions


def main():
    from mscoco_v2.dataset import Dataset

    train_data = Dataset(
            "/opt/data/VQA/v2_OpenEnded_mscoco_train2014_questions.json",
            "/opt/data/VQA/v2_mscoco_train2014_annotations.json"
    ).get_entries()
    val_data = Dataset(
            "/opt/data/VQA/v2_OpenEnded_mscoco_val2014_questions.json",
            "/opt/data/VQA/v2_mscoco_val2014_annotations.json"
    ).get_entries()

    train_data = to_questions(train_data)
    val_data = to_questions(val_data)

    nlp = spacy.load("en_core_web_sm",
                     disable=['parser', 'textcat', 'tagger', 'ner'])

    def tokenize(data):
        assert(isinstance(data, list))
        num_words = []
        for sentence in tqdm.tqdm(data, leave=False,
                                  desc="Spacy tokenize"):
            num_words.append(len(nlp(sentence)))
        return num_words

    train_words = tokenize(train_data)
    val_words = tokenize(val_data)

    try:
        import matplotlib.pyplot as plt
        plt.figure(figsize=(10, 4))

        plt.subplot(1, 2, 1)
        plt.hist(train_words, bins=list(range(max(train_words) + 2)),
                 density=False, facecolor='b', alpha=0.75)

        plt.xlabel("Words")
        plt.ylabel("Count")
        plt.title("Question length (VQA v2 train)")
        plt.grid(True)

        plt.subplot(1, 2, 2)
        plt.hist(val_words, bins=list(range(max(val_words) + 2)),
                 density=False, facecolor='b', alpha=0.75)

        plt.xlabel("Words")
        plt.ylabel("Count")
        plt.title("Question length (VQA v2 val)")
        plt.grid(True)

        plt.show()
    except ModuleNotFoundError as unused_e:
        unused_e = unused_e  # avoid unused error
        print("Install matplotlib for GUI stats.")


if __name__ == "__main__":
    sys.path.append(os.path.abspath(os.path.join(
        os.path.abspath(os.path.dirname(__file__)),
        "../"
    )))
    main()
