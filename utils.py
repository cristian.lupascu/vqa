import numpy as np
import mmap
import tqdm

from settings import GLOVE_PATH


def count_lines_in_file(file_path):
    lines = 0
    with open(file_path, "r") as f:
        # memory-map the file, size 0 means whole file
        buf = mmap.mmap(f.fileno(), 0, prot=mmap.PROT_READ)
        while buf.readline():
            lines += 1
    return lines


def load_GloVe():
    # Receives a line from a GloVe file (as string) and returns the word and
    # the embedding for that word.
    # Returns: pair (word: str, w_vector: np.ndarray)
    def parse_line(line, emb_dim):
        assert(isinstance(line, str))
        assert(isinstance(emb_dim, int))

        data = line.rstrip().split()
        assert(len(data) >= emb_dim)

        w_vector = None
        word = None
        if len(data) == emb_dim:
            # Whitespaces only followed by the embedding vector.
            # E.g. "           -0.36288 -0.075749 0.35433 -1.6601"
            idx = line.find(data[0])
            assert(idx >= 0 and idx < len(line))
            word = line[:idx]
            w_vector = data
        else:
            w_vector = data[-emb_dim:]
            word = data[:-emb_dim]
            # word can be multiple tokens (e.g. ['at', 'name@domain.com'])
            word = ' '.join(word).strip()

        assert(len(w_vector) == emb_dim)
        assert(len(word) >= 1)

        w_vector = np.asarray(w_vector, dtype='float32')
        assert(len(w_vector.shape) == 1)
        assert(w_vector.shape[0] == emb_dim)

        return word, w_vector

    num_lines = count_lines_in_file(GLOVE_PATH)
    embeddings = {}
    with open(GLOVE_PATH, "r") as f:
        for line in tqdm.tqdm(f, total=num_lines, desc="Parsing GloVe file"):
            word, w_vector = parse_line(line, 300)
            assert(w_vector.shape[0] == 300)
            embeddings[word] = w_vector
    return embeddings
