import configparser

from copy import deepcopy
from mscoco_v2 import settings


class Config(object):

    TRAIN_ANNOTATIONS_PATH = "mscoco_v2_train_annotations"
    VAL_ANNOTATIONS_PATH = "mscoco_v2_val_annotations"

    class __InnerConfig:
        def __init__(self):
            self.config = configparser.ConfigParser()
            self.config.read(settings.CONFIG_FILE)
            assert(Config.TRAIN_ANNOTATIONS_PATH in self.config)
            assert(Config.VAL_ANNOTATIONS_PATH in self.config)

            # Read top-N answers file.
            self.answers_index = {}
            idx = 0
            with open(settings.TOP_ANSWERS_FILE, 'r') as f:
                for line in f:
                    self.answers_index[line.rstrip()] = idx
                    idx += 1

        def get(self, key):
            if key == Config.TRAIN_ANNOTATIONS_PATH:
                return self.config[key]["Path"]
            elif key == Config.VAL_ANNOTATIONS_PATH:
                return self.config[key]["Path"]
            return None

        def get_answers_index(self):
            return deepcopy(self.answers_index)

    instance = None

    def __init__(self):
        if not Config.instance:
            Config.instance = Config.__InnerConfig()

    def get(self, key):
        good_key = (key in [
            Config.TRAIN_ANNOTATIONS_PATH,
            Config.VAL_ANNOTATIONS_PATH
        ])
        if not good_key:
            raise RuntimeError("Cannot find config entry: {}".format(key))
        return self.instance.get(key)

    def get_answers_index(self):
        return self.instance.get_answers_index()
