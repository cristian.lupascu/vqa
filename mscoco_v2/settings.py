import os


BASE_DIR = os.path.abspath(os.path.dirname(__file__))
CONFIG_FILE = os.path.join(BASE_DIR, "config.conf")
TOP_ANSWERS_FILE = os.path.join(BASE_DIR, "top_1000_answers.txt")
