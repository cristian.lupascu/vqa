import json
import random

from copy import deepcopy
from mscoco_v2.config import Config


class Dataset(object):

    def __init__(self, input_questions_path, annotations_path):
        assert(isinstance(input_questions_path, str))
        assert(isinstance(annotations_path, str))

        self.info = {}
        questions = self._read_input_questions(input_questions_path)
        annotations = self._read_annotations(annotations_path)
        assert(isinstance(questions, dict))
        assert(isinstance(annotations, dict))
        assert(len(questions) == len(annotations))

        assert(set(questions.keys()) == set(annotations.keys()))
        assert(set(questions.keys()).issubset(set(annotations.keys())))
        assert(set(annotations.keys()).issubset(set(questions.keys())))

        # Group (question_id, image_id) -> (question, answer).
        entries = {}
        for key in questions:
            assert(key in annotations)
            answer = annotations[key]
            question = questions[key]
            assert(isinstance(answer, str))
            assert(isinstance(question, str))
            entries[key] = (question, answer)
            annotations.pop(key)
        assert(len(entries) == len(questions))
        assert(len(annotations) == 0)
        del annotations
        del questions

        # Filter questions to contain only those with answers in top-N.
        answer_index = Config().get_answers_index()
        self.questions = {}
        for key, value in entries.items():
            assert(len(key) == 2)
            assert(len(value) == 2)
            answer = value[1]
            if answer in answer_index:
                self.questions[key] = value
        self.info["kept"] = 100.0 * len(self.questions) / len(entries)

    def _read_input_questions(self, input_questions_path):
        obj = None
        with open(input_questions_path, "r") as f:
            obj = json.load(f)
        if obj is None:
            raise RuntimeError("Cannot load JSON from {}".format(input_questions_path))  # noqa: E501

        assert("info" in obj)
        assert("task_type" in obj)
        assert("data_type" in obj)
        assert("data_subtype" in obj)
        assert("questions" in obj)
        assert("license" in obj)
        assert(len(obj) == 6)

        info = obj["info"]
        info["task_type"] = obj["task_type"]
        info["data_type"] = obj["data_type"]
        info["data_subtype"] = obj["data_subtype"]
        info["license"] = obj["license"]

        all_questions = obj["questions"]
        assert(isinstance(all_questions, list))
        info["nr_questions"] = len(all_questions)

        question_ids = set()
        for question in all_questions:
            assert(len(question) == 3)
            assert("image_id" in question)
            assert("question_id" in question)
            assert("question" in question)
            assert(isinstance(question["question"], str))
            assert(isinstance(question["image_id"], int))
            assert(isinstance(question["question_id"], int))
            assert(question["question_id"] not in question_ids)
            question_ids.add(question["question_id"])
        assert(len(question_ids) == len(all_questions))
        self.info["questions"] = info

        res = {}  # dict (question_id, image_id) -> question
        for question in all_questions:
            question_id = question["question_id"]
            image_id = question["image_id"]
            res[(question_id, image_id)] = question["question"]
        assert(len(res) == len(all_questions))

        return res

    def _read_annotations(self, annotations_path):
        obj = None
        with open(annotations_path, "r") as f:
            obj = json.load(f)
        if obj is None:
            raise RuntimeError("Cannot load JSON from {}".format(annotations_path))  # noqa: E501

        assert("info" in obj)
        assert("data_type" in obj)
        assert("data_subtype" in obj)
        assert("annotations" in obj)
        assert("license" in obj)
        assert(len(obj) == 5)

        info = obj["info"]
        info["data_type"] = obj["data_type"]
        info["data_subtype"] = obj["data_subtype"]
        info["license"] = obj["license"]

        all_annotations = obj["annotations"]
        assert(isinstance(all_annotations, list))
        info["nr_annotations"] = len(all_annotations)

        question_ids = set()
        for annotation in all_annotations:
            assert("question_id" in annotation)
            assert("image_id" in annotation)
            assert("question_type" in annotation)
            assert("answer_type" in annotation)
            assert("answers" in annotation)
            assert("multiple_choice_answer" in annotation)
            assert(len(annotation) == 6)

            # multiple_choice_answer: most frequent ground-truth answer.
            multiple_choice_answer = annotation["multiple_choice_answer"]
            assert(isinstance(annotation["answers"], list))
            assert(len(annotation["answers"]) >= 1)

            found = False
            for answer in annotation["answers"]:
                assert(len(answer) == 3)
                assert("answer_id" in answer)
                assert("answer" in answer)
                assert("answer_confidence" in answer)
                if answer["answer"] == multiple_choice_answer:
                    found = True
            assert(found is True)

            assert(annotation["question_id"] not in question_ids)
            question_ids.add(annotation["question_id"])
        assert(len(question_ids) == len(all_annotations))
        self.info["annotations"] = info

        res = {}  # dict (question_id, image_id) -> most frequent answer.
        for annotation in all_annotations:
            question_id = annotation["question_id"]
            image_id = annotation["image_id"]
            multiple_choice_answer = annotation["multiple_choice_answer"]
            res[(question_id, image_id)] = multiple_choice_answer
        assert(len(res) == len(all_annotations))

        return res

    def get_entries(self):
        assert(isinstance(self.questions, dict))
        return deepcopy(self.questions)

    def show_sample(self, cnt=10):
        assert(cnt >= 1)
        keys = random.sample(list(self.questions.keys()), cnt)
        subset = {}
        for key in keys:
            subset[str(key)] = self.questions[key]
        assert(len(subset) == cnt)
        print(json.dumps(subset, indent=4, sort_keys=True))

    def __str__(self):
        return json.dumps(self.info, indent=4, sort_keys=True)
