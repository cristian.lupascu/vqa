import json
import run_as_main

from mscoco_v2.config import Config


def process_answer(answer):
    assert(isinstance(answer, str))
    return answer


def get_answers(annotations_path):
    obj = None
    with open(annotations_path, "r") as f:
        obj = json.load(f)
    if obj is None:
        raise RuntimeError("Cannot load JSON from {}".format(annotations_path))  # noqa: E501

    assert("info" in obj)
    assert("data_type" in obj)
    assert("data_subtype" in obj)
    assert("annotations" in obj)
    assert("license" in obj)
    assert(len(obj) == 5)

    all_annotations = obj["annotations"]
    assert(isinstance(all_annotations, list))

    question_ids = set()
    all_answers = {}
    for annotation in all_annotations:
        assert("question_id" in annotation)
        assert("image_id" in annotation)
        assert("question_type" in annotation)
        assert("answer_type" in annotation)
        assert("answers" in annotation)
        assert("multiple_choice_answer" in annotation)
        assert(len(annotation) == 6)

        # multiple_choice_answer: most frequent ground-truth answer.
        multiple_choice_answer = annotation["multiple_choice_answer"]
        assert(isinstance(annotation["answers"], list))
        assert(len(annotation["answers"]) >= 1)

        found = False
        for answer in annotation["answers"]:
            assert(len(answer) == 3)
            assert("answer_id" in answer)
            assert("answer" in answer)
            assert("answer_confidence" in answer)
            if answer["answer"] == multiple_choice_answer:
                found = True
        assert(found is True)

        assert(annotation["question_id"] not in question_ids)
        question_ids.add(annotation["question_id"])

        answer = process_answer(multiple_choice_answer)
        all_answers[answer] = all_answers.get(answer, 0) + 1
    assert(len(question_ids) == len(all_annotations))

    return len(question_ids), all_answers


def main():
    config = Config()
    all_answers = {}
    paths = [
            config.get(Config.TRAIN_ANNOTATIONS_PATH),
            config.get(Config.VAL_ANNOTATIONS_PATH)
    ]
    num_questions = 0
    for path in paths:
        count, answers = get_answers(path)
        num_questions += count
        for answer, count in answers.items():
            all_answers[answer] = all_answers.get(answer, 0) + count
    all_answers = list(all_answers.items())
    all_answers = sorted(all_answers, key=lambda x: x[1], reverse=True)
    assert(num_questions == sum([y for _, y in all_answers]))

    thresholds = [2, 500, 900, 1000, 1100, 1200, 1300, 1400, 1500, 2000, 3000]
    coverage = [0]
    total = 0
    for _, cnt in all_answers:
        total += cnt
        x = 100.0 * total / num_questions
        coverage.append(x)
        if len(coverage) - 1 in thresholds:
            print("With {} answers: {}%".format(len(coverage) - 1, x))

    # Keep only most common 1000 answers.
    all_answers = all_answers[:1000]
    all_answers = [x for x, _ in all_answers]
    all_answers = sorted(all_answers)
    assert(len(all_answers) == 1000)
    with open("top_1000_answers.txt", "w") as g:
        for answer in all_answers:
            g.write(answer)
            g.write("\n")
        g.flush()


if __name__ == "__main__":
    run_as_main.void()
    main()
