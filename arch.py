import tensorflow as tf

from keras.layers import Input, Embedding, GRU, Dense, Dropout, Reshape
from keras.layers import Concatenate
from keras.models import Model
from layers import StackedAttentionVQA
from layers.advanced_activations import GELU
from settings import QUESTION_LEN, BATCH_SIZE


# This function builds a model that takes as inputs the already embedded
# image and question. It returns the final prediction. This model will be used
# as the final layer in the complete model with embeddings.
# This is a hack used to not save the embeddings weights.
def get_main_model(question_embedding_size, batch_size=BATCH_SIZE,
                   return_attention_weights=False):
    # Here starts the main model (ignoring the embedding layers).
    embedded_image_input = Input(
                shape=(7 * 7 * 512,),
                name="embedded_image_input"
    )
    embedded_question_input = Input(
                shape=(QUESTION_LEN, question_embedding_size),
                name="embedded_question_input"
    )
    embedded_features_input = Input(
                shape=(100, 2048),
                name="embedded_features_input"
    )

    question = GRU(300, return_sequences=True, name="vqa_gru_1",
                   dropout=0.15, recurrent_dropout=0.15)(embedded_question_input)  # noqa: E501
    question = GRU(300, dropout=0.15, recurrent_dropout=0.15,
                   name="vqa_gru_2")(question)

    # Reshape image into its original shape.
    image = Reshape((7, 7, 512), name="vqa_reshape_img")(embedded_image_input)
    image = Dropout(0.2, name="vqa_image_input_dropout")(image)
    question = Dropout(0.2, name="vqa_question_dropout")(question)

    x = StackedAttentionVQA(
        attention_size=768,
        batch_size=batch_size,
        return_attention_weights=return_attention_weights,
        name="vqa_stacked_attention",
    )([image, question])

    if return_attention_weights:
        return Model(inputs=[embedded_image_input, embedded_question_input],
                     outputs=x)

    features = Reshape((100, 1, 2048), name="vqa_reshape_features")(embedded_features_input)  # noqa: E501
    features = StackedAttentionVQA(
        attention_size=512,
        batch_size=batch_size,
        return_attention_weights=False,
        name="vqa_stacked_attention_features",
    )([features, question])

    x = Concatenate(axis=-1, name="vqa_concatenate_all")([x, features])

    x = Dropout(0.2, name="vqa_dropout_1")(x)
    x = Dense(300, activation=None, name="vqa_dense_1")(x)
    x = GELU(name="vqa_gelu_1")(x)

    x = Dropout(0.2, name="vqa_dropout_2")(x)
    x = Dense(300, activation=None, name="vqa_dense_2")(x)
    x = GELU(name="vqa_gelu_2")(x)

    x = Dropout(0.2, name="vqa_dropout_3")(x)
    output = Dense(1000, activation='softmax', name="vqa_dense_out")(x)
    model = Model(inputs=[embedded_image_input, embedded_question_input, embedded_features_input],  # noqa: E501
                  outputs=output)
    return model


def model_from_features(question_embeddings):
    main_model = get_main_model(question_embeddings.shape[1])

    # Now, let's build the full model (including embeddings).
    # Load embeddings on the CPU since the image embeddings are huge (about
    # 50GB) and they will not fit the GPU.
    with tf.device('/cpu:0'):
        image_input = Input(shape=(7, 7, 512), name="image_input")
        question_input = Input(shape=(QUESTION_LEN,), name="question_input",
                               dtype="int32")
        features_input = Input(shape=(100, 2048), name="features_input")

        reshaped_image = Reshape((7 * 7 * 512,))(image_input)

        # Embed question.
        embedded_question = Embedding(
                            question_embeddings.shape[0],
                            question_embeddings.shape[1],
                            weights=[question_embeddings],
                            input_length=QUESTION_LEN,
                            trainable=False
        )(question_input)

    output = main_model([reshaped_image, embedded_question, features_input])
    full_model = Model(inputs=[image_input, question_input, features_input],
                       outputs=output)

    return full_model, main_model


def model_for_prediction(question_embeddings):
    main_model = get_main_model(question_embeddings.shape[1], batch_size=1)

    image_input = Input(shape=(7, 7, 512), name="image_input")
    question_input = Input(shape=(QUESTION_LEN,), name="question_input",
                           dtype="int32")

    reshaped_image = Reshape((7 * 7 * 512,))(image_input)

    # Embed question.
    embedded_question = Embedding(
                        question_embeddings.shape[0],
                        question_embeddings.shape[1],
                        weights=[question_embeddings],
                        input_length=QUESTION_LEN,
                        trainable=False
    )(question_input)

    output = main_model([reshaped_image, embedded_question])
    full_model = Model(inputs=[image_input, question_input],
                       outputs=output)

    return full_model, main_model


def model_for_heatmap(question_embeddings):
    main_model = get_main_model(question_embeddings.shape[1], batch_size=1,
                                return_attention_weights=True)

    image_input = Input(shape=(7, 7, 512), name="image_input")
    question_input = Input(shape=(QUESTION_LEN,), name="question_input",
                           dtype="int32")

    reshaped_image = Reshape((7 * 7 * 512,))(image_input)

    # Embed question.
    embedded_question = Embedding(
                        question_embeddings.shape[0],
                        question_embeddings.shape[1],
                        weights=[question_embeddings],
                        input_length=QUESTION_LEN,
                        trainable=False
    )(question_input)

    output = main_model([reshaped_image, embedded_question])
    full_model = Model(inputs=[image_input, question_input],
                       outputs=output)

    return full_model, main_model
