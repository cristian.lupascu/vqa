import arch
import argparse
import cv2
import keras
import numpy as np
import os

from PIL import Image
from keras.applications import vgg19
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.preprocessing.sequence import pad_sequences
from mscoco_v2.config import Config
from settings import QUESTION_LEN, IMAGE_SIZE
from tokenizers.spacy_tokenizer import SpaCyTokenizer
from utils import load_GloVe

MODEL_WEIGHTS = "model-epoch0081-1.54438-0.49322.h5"


def _preprocess(question, img_path, tokenizer):
    assert(isinstance(question, str))
    assert(isinstance(img_path, str))

    model = vgg19.VGG19(include_top=False, weights='imagenet',
                        input_shape=(224, 224, 3), pooling='none')

    img = load_img(img_path, target_size=(224, 224))
    img = img_to_array(img)
    img = np.expand_dims(img, 0)
    img = vgg19.preprocess_input(np.array(img).copy())
    assert(img.shape == (1, IMAGE_SIZE, IMAGE_SIZE, 3))
    image = model.predict(img)

    questions = [question]
    questions = tokenizer.texts_to_sequences(questions)
    questions = pad_sequences(questions, maxlen=QUESTION_LEN)

    print("Questions: {}, dtype = {}".format(questions.shape, questions.dtype))
    print("Image: {}, dtype = {}".format(image.shape, image.dtype))

    return {
            "question_input": questions,
            "image_input": image
    }


def _build_embedding_matrix(word_index, glove):
    num_words = len(word_index) + 1
    emb_matrix = np.zeros((num_words, 300))
    for word, i in word_index.items():
        if word in glove:
            emb_matrix[i] = glove[word]
        elif word.lower() in glove:
            emb_matrix[i] = glove[word.lower()]
        elif word.upper() in glove:
            emb_matrix[i] = glove[word.upper()]
        else:
            # words not found in embedding index will be all-zeros.
            print("Unknown embedding for word: {}".format(word))
    return emb_matrix


def predict(question, img_path, glove):
    assert(isinstance(question, str))
    assert(isinstance(img_path, str))

    tokenizer = SpaCyTokenizer()
    all_sentences = [question]
    tokenizer.fit_on_texts(all_sentences)

    data = _preprocess(question, img_path, tokenizer)

    word_index = tokenizer.word_index()
    print('Found %s unique tokens.' % len(word_index))

    emb_matrix = _build_embedding_matrix(word_index, glove)
    full_model, main_model = arch.model_for_prediction(emb_matrix)
    full_model.compile(loss=keras.losses.categorical_crossentropy,
                       optimizer=keras.optimizers.Adam(),
                       metrics=['accuracy'])
    main_model.summary()
    main_model.load_weights(MODEL_WEIGHTS)
    y = full_model.predict(data, batch_size=1, verbose=0)

    answer_index = Config().get_answers_index()
    answer_index = {v: k for k, v in answer_index.items()}
    assert(y.shape == (1, len(answer_index)))
    y = y[0]

    answers = []
    for i in range(0, y.shape[0]):
        answers.append((answer_index[i], y[i]))
    answers = sorted(answers, key=lambda x: x[1], reverse=True)

    print("")
    print(question)
    idx = 1
    for answer, score in answers[:10]:
        print("{} {} (confidence = {}%)".format(
                    (str(idx) + ".").ljust(3, ' '),
                    answer.ljust(30, ' '),
                    "{0:.3f}".format(100.0 * score)))
        idx += 1
    print("")


def heat_map(question, img_path, glove):
    assert(isinstance(question, str))
    assert(isinstance(img_path, str))

    tokenizer = SpaCyTokenizer()
    all_sentences = [question]
    tokenizer.fit_on_texts(all_sentences)

    data = _preprocess(question, img_path, tokenizer)

    word_index = tokenizer.word_index()
    print('Found %s unique tokens.' % len(word_index))

    emb_matrix = _build_embedding_matrix(word_index, glove)
    full_model, main_model = arch.model_for_heatmap(emb_matrix)
    full_model.compile(loss=keras.losses.categorical_crossentropy,
                       optimizer=keras.optimizers.Adam(),
                       metrics=['accuracy'])
    main_model.summary()
    main_model.load_weights(MODEL_WEIGHTS, by_name=True)

    y = full_model.predict(data, batch_size=1, verbose=0)
    assert(len(y.shape) == 3)
    assert(y.shape[0] == 1)
    y = y[0]
    assert(y.shape == (7, 7))
    y = y / np.amax(y) * 255.0

    img = Image.open(img_path)
    img = img.convert('RGB')  # Make sure the image is RGB.
    img.show()

    heatmap = Image.fromarray(y).resize(img.size, Image.BILINEAR)
    heatmap = heatmap.convert('RGB')
    heatmap = cv2.cvtColor(np.array(heatmap), cv2.COLOR_RGB2BGR)
    heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_JET)
    # Image.fromarray(cv2.cvtColor(np.array(heatmap), cv2.COLOR_BGR2RGB)).show()  # noqa: E501

    img = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
    img = cv2.addWeighted(heatmap, 0.6, img, 0.4, 0)
    Image.fromarray(cv2.cvtColor(np.array(img), cv2.COLOR_BGR2RGB)).show()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--img', type=str, required=True,
                        help='A path to the image to be processed.')
    parser.add_argument('-q', '--question', type=str, required=True,
                        help='The question to be asked.')
    FLAGS, _ = parser.parse_known_args()
    img_path = FLAGS.img
    question = FLAGS.question
    glove = load_GloVe()
    predict(question, img_path, glove)
    heat_map(question, img_path, glove)


if __name__ == "__main__":
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    main()
