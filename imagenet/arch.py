import tensorflow as tf

from keras.layers import Input, Dense, Flatten
from keras.layers import AveragePooling2D, Conv2D, BatchNormalization
from keras.models import Model
from layers.advanced_activations import GELU
from models import Minet
from imagenet.settings import IMAGE_SIZE


def conv2d_bn(input_tensor, num_filters, kernel_size, name,
              strides=(1, 1), padding='same', trainable=True):
    assert(isinstance(num_filters, int))
    assert(num_filters >= 1)
    assert(isinstance(kernel_size, int))
    assert(kernel_size >= 1)
    assert(isinstance(name, str))
    assert(isinstance(strides, tuple))
    assert(len(strides) == 2)
    assert(padding in ['valid', 'same'])

    x = Conv2D(
            num_filters, (kernel_size, kernel_size),
            strides=strides,
            padding=padding,
            activation=None,
            use_bias=True,
            name=name + "_conv2D",
            trainable=trainable)(input_tensor)
    # When the next layer is linear (also ReLU), scaling can be disabled
    # since the scaling will be done by the next layer.
    x = BatchNormalization(axis=3, scale=False, name=name + "_batch_norm",
                           trainable=trainable)(x)
    x = GELU(name=name + "_activation", trainable=trainable)(x)
    return x


def get_model():
    model = None
    with tf.device('/cpu:0'):
        model_input = Input(shape=(IMAGE_SIZE, IMAGE_SIZE, 3))
        x = Minet(model_input)
        x = AveragePooling2D(
                3, strides=(2, 2), padding='same',
                name="pre_train_image_net_avg_pooling_1"
        )(x)
        x = conv2d_bn(x, 16, 1, padding='same',
                      name='pre_train_image_net_conv1')
        x = Flatten(name="pre_train_image_net_flatten")(x)
        x = Dense(256, activation='relu', name="pre_train_image_net_dense1")(x)
        output = Dense(996, activation='softmax',
                       name="pre_train_image_net_output")(x)
        model = Model(inputs=model_input, outputs=output)
    return model
