import json
import os
import random
import shutil
import urllib.request

from threading import Thread

NUM_THREADS = 32
PERCENTAGE = 0.4  # out of 1.0


def download_class(cls, urls):
    directory = os.path.join("data", cls)

    idx = 0
    urls = [x.strip() for x in urls]
    random.shuffle(urls)
    size = max(1, int(PERCENTAGE * len(urls)))
    urls = urls[:size]
    for url in urls:
        fname = str(idx)
        extension = url.split(".")[-1].lower()
        if extension in ['jpg', 'gif', 'jpeg', 'png', 'bmp', 'thb',
                         'jpe', 'tif', 'pjpeg']:
            fname = fname + "." + extension
        else:
            fname = fname + ".jpg"

        fname = os.path.join(directory, fname)
        try:
            urllib.request.urlretrieve(url, filename=fname)
        except Exception as ex:
            if False:
                print("Cannot fetch image {}; {}".format(url, str(ex)))

        idx += 1


def download(images):
    idx = 1
    for cls, urls in images.items():
        download_class(cls, urls)
        print("Downloaded {} ({}/{}).".format(cls, idx, len(images)),
              flush=True)
        idx += 1


def main():
    # A human readable class to a WordNet ID (e.g. 'scuba_diver': 'n10565667')
    wnid_to_class = {}
    with open("imagenet_class_index.json", "r") as f:
        class_index = json.load(f)
        assert(len(class_index) == 1000)
        for i in range(0, 1000):
            assert(str(i) in class_index)
            wnid, class_name = class_index[str(i)]
            wnid_to_class[wnid] = class_name.lower()

    assert(len(wnid_to_class.keys()) == 1000)

    images = {}
    unknown_class = 0
    total = 0
    with open("fall11_urls.txt", "r", encoding="latin-1") as f:
        for line in f:
            tokens = line.rstrip().split('\t')
            if len(tokens) > 2:
                continue
            assert(len(tokens) == 2)

            total += 1
            wnid = tokens[0]
            wnid = wnid[:wnid.index('_')]
            assert(wnid[0] == 'n')
            url = tokens[1]
            if wnid not in wnid_to_class:
                unknown_class += 1
                continue
            assert(wnid in wnid_to_class)
            cls = wnid_to_class[wnid]

            if cls not in images:
                images[cls] = set()
            images[cls].add(url.strip())

    print("Using {0:.3f}% of the entire ImageNet.".format(
        100.0 * (total - unknown_class) / total))
    print("Total {} images".format(total - unknown_class))

    for cls, urls in images.items():
        print("Class {} has {} images.".format(cls, len(urls)))

    # Create all directories.
    for cls in images:
        directory = os.path.join("data", cls)
        if os.path.exists(directory):
            shutil.rmtree(directory)
        os.mkdir(directory)

    classes = list(images.keys())
    chunk_size = int(len(classes) / NUM_THREADS)
    loads = []
    for i in range(0, NUM_THREADS):
        if i == NUM_THREADS - 1:
            chunk_size = len(classes)
        loads.append(classes[:chunk_size])
        classes = classes[chunk_size:]
    assert(len(classes) == 0)
    assert(sum([len(x) for x in loads]) == len(images))
    assert(len(list(set().union(*loads))) == len(images))

    tasks = []
    for load in loads:
        task = {}
        for cls in load:
            task[cls] = images[cls]
        tasks.append(task)
    assert(len(tasks) == NUM_THREADS)

    threads = []
    for i in range(0, NUM_THREADS):
        thread = Thread(target=download, args=(tasks[i],))
        threads.append(thread)
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()


if __name__ == "__main__":
    main()
