import numpy as np
import os
import tqdm

from PIL import Image


# Some images show a large white background with the following
# message: "This photo is no longer available". Identify them
# by the white backgound.
def is_unavailable(img):
    data = np.asarray(img)
    assert(len(data.shape) == 3)
    data = np.prod(data, axis=2)
    threshold = int(255 * 255 * 255 * 0.8)
    cnt = np.count_nonzero(data >= threshold)
    maximum = data.shape[0] * data.shape[1]
    return 1.0 * cnt / maximum >= 0.80


def main():
    all_files = []
    classes = set()
    for directory in os.scandir("data/"):
        cls = directory.name
        classes.add(cls)
        files = [f for f in os.listdir(directory.path)]
        files = [os.path.join(directory.path, f) for f in files]
        all_files += files
    for cls in classes:
        try:
            os.mkdir(os.path.join("processed_data", cls))
        except:
            pass

    bad_images = 0
    good_images = 0
    for img_path in tqdm.tqdm(all_files):
        try:
            img = Image.open(img_path)
            img = img.convert('RGB')  # Make sure the image is RGB.
            img = img.resize((224, 224), resample=Image.BILINEAR)
            if is_unavailable(img):
                raise RuntimeError("Image shows unavailable message")
            filename, file_extension = os.path.splitext(img_path)
            assert(file_extension.startswith("."))
            parts = list(os.path.split(filename))
            parts = list(os.path.split(parts[0])) + parts[1:]
            filename = os.path.join("processed_data", parts[1], parts[2])
            img.save(filename + '.jpg')
            good_images += 1
        except Exception as ex:
            # Print error and remove bad file.
            print("Error at image: {} - {}".format(img_path, str(ex)))
            # os.remove(img_path)
            bad_images += 1
    print("Bad images: {}".format(bad_images))
    print("Good images: {}".format(good_images))


if __name__ == "__main__":
    main()
