import keras
import run_as_main
import tensorflow as tf

from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import multi_gpu_model
from shared.keras.callbacks import SaveParallelModel
from imagenet import arch
from imagenet.settings import IMAGE_SIZE, TRAINING_DIR

BATCH_SIZE = 39
NUM_TRAIN_IMAGES = 258553
NUM_VAL_IMAGES = 64142
NUM_GPUS = 3


def main():
    datagen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            zca_epsilon=1e-06,  # epsilon for ZCA whitening
            rotation_range=25.0,  # randomly rotate images (degrees)
            # randomly shift images horizontally (fraction of total width)
            width_shift_range=0.15,
            # randomly shift images vertically (fraction of total height)
            height_shift_range=0.15,
            shear_range=0.0,  # set range for random shear
            zoom_range=0.15,  # set range for random zoom
            channel_shift_range=0.0,  # set range for random channel shifts
            # set mode for filling points outside the input boundaries
            fill_mode='nearest',
            cval=0.0,  # value used for fill_mode = "constant"
            horizontal_flip=True,  # randomly flip images
            vertical_flip=False,  # randomly flip images
            # set rescaling factor (applied before any other transformation)
            rescale=1.0 / 255.0,
            # set function that will be applied on each input
            preprocessing_function=None,
            # image data format, either "channels_first" or "channels_last"
            data_format="channels_last",
            # fraction of images reserved for validation (between 0 and 1)
            validation_split=0.2
    )

    train_generator = datagen.flow_from_directory(
            TRAINING_DIR, target_size=(IMAGE_SIZE, IMAGE_SIZE),
            color_mode='rgb', shuffle=True, interpolation='bilinear',
            class_mode='categorical', batch_size=BATCH_SIZE,
            subset="training"
    )
    val_generator = datagen.flow_from_directory(
            TRAINING_DIR, target_size=(IMAGE_SIZE, IMAGE_SIZE),
            color_mode='rgb', shuffle=True, interpolation='bilinear',
            class_mode='categorical', batch_size=BATCH_SIZE,
            subset='validation'
    )

    model = arch.get_model()
    parallel_model = model
    if NUM_GPUS > 1:
        parallel_model = multi_gpu_model(model, gpus=NUM_GPUS)
    parallel_model.compile(
                  loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adam(),
                  metrics=['accuracy'])
    model.summary()

    config = tf.ConfigProto(
            allow_soft_placement=True,
            log_device_placement=False
    )
    sess = tf.Session(config=config)
    K.set_session(sess)
    sess.run(tf.global_variables_initializer())
    sess.run(tf.tables_initializer())

    # model.load_weights("model-epoch0016-2.98129-0.37225.h5", by_name=True)
    save_model = SaveParallelModel(model)
    parallel_model.fit_generator(
            train_generator,
            steps_per_epoch=NUM_TRAIN_IMAGES / BATCH_SIZE,
            epochs=100,
            validation_data=val_generator,
            validation_steps=NUM_VAL_IMAGES / BATCH_SIZE,
            verbose=2,
            callbacks=[save_model]
    )


if __name__ == "__main__":
    assert(BATCH_SIZE % NUM_GPUS == 0)
    run_as_main.void()
    main()
