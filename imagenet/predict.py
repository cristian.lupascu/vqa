import argparse
import keras
import numpy as np
import os
import run_as_main

from PIL import Image
from vqa.imagenet import arch
from vqa.imagenet.settings import IMAGE_SIZE, TRAINING_DIR


def get_classes():
    # From keras docs:
    # "If not provided, the list of classes will be automatically inferred
    # from the subdirectory names/structure under directory, where each
    # subdirectory will be treated as a different class (and the order of the
    # classes, which will map to the label indices, will be alphanumeric).
    subdirs = os.listdir(TRAINING_DIR)
    subdirs = [x for x in subdirs if os.path.isdir(os.path.join(TRAINING_DIR, x))]  # noqa: E501
    subdirs = sorted(list(subdirs))
    classes = {}
    for i in range(0, len(subdirs)):
        classes[i] = subdirs[i]
    return classes


def main(img_path):
    img = Image.open(img_path)
    img = img.convert('RGB')  # Make sure the image is RGB.
    img = img.resize((IMAGE_SIZE, IMAGE_SIZE), resample=Image.BILINEAR)
    img = np.asarray(img, dtype=np.float32) / 255.0
    img = np.expand_dims(img, axis=0)
    assert(img.shape == (1, IMAGE_SIZE, IMAGE_SIZE, 3))

    model = arch.get_model()
    model.compile(
            loss=keras.losses.categorical_crossentropy,
            optimizer=keras.optimizers.Adam(),
            metrics=['accuracy']
    )
    model.load_weights("model-epoch0016-2.98129-0.37225.h5", by_name=True)

    y = np.squeeze(model.predict(img, batch_size=1, verbose=0))
    assert(len(y.shape) == 1)
    n = y.shape[0]
    scores = []
    for i in range(0, n):
        scores.append((y[i], i))
    scores = sorted(scores, key=lambda x: x[0], reverse=True)

    classes = get_classes()
    assert(len(classes) == n)

    print("")
    print("Predictions:")
    for score, idx in scores[:5]:
        print("{}% {}".format("{0:.3f}".format(100.0 * score), classes[idx]))


if __name__ == "__main__":
    run_as_main.void()
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--img', type=str, required=True,
                        help='A path to the image to be processed.')
    FLAGS, _ = parser.parse_known_args()
    main(FLAGS.img)
