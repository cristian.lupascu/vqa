import base64
import numpy as np
import csv


def read_features(infile):
    FIELDNAMES = [
            'image_id', 'image_w', 'image_h',
            'num_boxes', 'boxes', 'features'
    ]
    in_data = {}
    with open(infile, "r") as tsv_in_file:
        reader = csv.DictReader(tsv_in_file, delimiter='\t',
                                fieldnames=FIELDNAMES)
        for item in reader:
            item['image_id'] = int(item['image_id'])
            item['image_h'] = int(item['image_h'])
            item['image_w'] = int(item['image_w'])
            item['num_boxes'] = int(item['num_boxes'])
            for field in ['boxes', 'features']:
                item[field] = np.frombuffer(
                        base64.decodestring(item[field].encode()),
                        dtype=np.float32).reshape((item['num_boxes'], -1))
            in_data[item['image_id']] = item['features']
    return in_data
