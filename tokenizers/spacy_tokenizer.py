import json
import types
import spacy

from keras.preprocessing.text import Tokenizer as KerasTok
from tokenizers.tokenizer import Tokenizer
from tqdm import tqdm


class SpaCyTokenizer(Tokenizer):

    def __init__(self):
        self.tokenizer = KerasTok(
                num_words=None,
                filters='',
                lower=False,
                split=' ',
                char_level=False,
                oov_token='UNK',
                document_count=0
        )
        # Dictionary mapping strings to spacy documents.
        self.cache = {}
        self.nlp = spacy.load("en_core_web_sm",
                              disable=['parser', 'textcat', 'tagger', 'ner'])

        # Default batch_size and number of threads used for tokenization.
        self.batch_size = 150
        self.n_threads = 4

        super(SpaCyTokenizer, self).__init__()

    def tokenize_as_str(self, text):
        assert(isinstance(text, str))
        doc = None
        if text in self.cache:
            doc = self.cache[text]
        else:
            doc = self.nlp(text)
        return ' '.join([str(token) for token in doc])

    def announce_batch_tokenization(self, texts):
        texts = [text for text in texts if text not in self.cache]
        if len(texts) == 0:
            return

        # Preprocess the texts in batch mode and cache them.
        docs = self.nlp.pipe(
                texts,
                batch_size=self.batch_size,
                n_threads=self.n_threads
        )
        assert(isinstance(docs, types.GeneratorType))

        idx = 0
        for doc in tqdm(docs, total=len(texts), desc="SpaCy Tokenization"):
            self.cache[texts[idx]] = doc
            idx += 1
        assert(idx == len(texts))

    # Texts will be processed in batch of @batch_size.
    def set_batch_size(self, batch_size):
        assert(batch_size >= 1)
        self.batch_size = batch_size

    def get_batch_size(self):
        return self.batch_size

    # Set number of threads that will be used for tokenization.
    def set_nthreads(self, n_threads):
        assert(n_threads == -1 or n_threads >= 1)
        self.n_threads = n_threads

    def get_nthreads(self):
        return self.n_threads

    def __str__(self):
        out = "SpaCy Tokenizer {\n"
        out += "    Batch size: " + str(self.batch_size) + '\n'
        out += "    Num threads: " + str(self.n_threads) + '\n'
        out += "    Language model properties: "
        out += json.dumps(self.nlp.meta, indent=8, sort_keys=True)
        out = out[0:-2] + "\n    }\n"
        out += "}\n"
        return out
