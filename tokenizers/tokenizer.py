from keras.preprocessing.text import Tokenizer as KerasTok


class Tokenizer(object):

    def __init__(self):
        # self.tokenizer must be set by the classes that extend Tokenizer.
        assert(isinstance(self.tokenizer, KerasTok))
        self.is_fit = False
        self.cached_inverted_word_index = None

    # Receives a string and returns another string. The returned string
    # will be further tokenized using self.tokenizer and should be computed
    # given the contract that it will be fed as an input to self.tokenizer.
    def tokenize_as_str(self, text):
        raise NotImplementedError("Overwrite Tokenizer.tokenize!")

    # Before calling tokenize_as_str for large number of texts, this function
    # will be called. It is advisable to overwrite this method and maybe do
    # some batch processing of texts. This would highly bring important
    # speed-ups especially for contexts such as spaCy where we can take
    # advantage of direct batching and parallelism. The @texts argument here
    # is the same as the one in fit_on_texts and texts_to_sequences. Be aware
    # that there is no gurantee that no new sentences can be tokenized. One
    # should use this method to build a cache but if a text cannot be found in
    # cache then it has to be process individually and a valid answer (not
    # None) must be returned to the caller.
    def announce_batch_tokenization(self, texts):
        raise NotImplementedError(
                    "Overwrite Tokenizer.announce_batch_tokenization!")

    ####################################################################
    #        No method after this line should be overwritten.          #
    ####################################################################

    # Receives a List[String] and fits the model using the sentences.
    def fit_on_texts(self, texts):
        assert(isinstance(texts, list))
        assert(isinstance(self.is_fit, bool))
        if self.is_fit:
            raise RuntimeError("Cannot call fit_on_texts() twice!")

        # Process each text. After that call self.tokenizer.fit_on_texts.
        self.announce_batch_tokenization(texts)
        processed_texts = []
        for text in texts:
            processed_text = self.tokenize_as_str(text)
            assert(isinstance(processed_text, str))
            processed_texts.append(processed_text)

        assert(len(texts) == len(processed_texts))

        self.tokenizer.fit_on_texts(processed_texts)
        self.is_fit = True

    # Receives a List[String] and returns a List[int].
    def texts_to_sequences(self, texts):
        assert(isinstance(texts, list))
        assert(isinstance(self.is_fit, bool))
        if not self.is_fit:
            raise RuntimeError("You have to call fit_on_texts() before!")

        # Process each text. After that call self.tokenizer.texts_to_sequences
        self.announce_batch_tokenization(texts)
        processed_texts = []
        for text in texts:
            processed_text = self.tokenize_as_str(text)
            assert(isinstance(processed_text, str))
            processed_texts.append(processed_text)

        assert(len(texts) == len(processed_texts))

        seqs = self.tokenizer.texts_to_sequences(processed_texts)
        assert(isinstance(seqs, list))
        assert(len(seqs) == len(texts))
        return seqs

    # Returns a dictionary mapping words (str) to their rank/index (int).
    # Can be called only set after fit_on_texts was called.
    def word_index(self):
        if not self.is_fit:
            raise RuntimeError("You have to call fit_on_texts() before!")
        word_index = self.tokenizer.word_index
        assert(isinstance(word_index, dict))
        return word_index

    # Returns a dictionary mapping ranks/indexes (int) to their word (string).
    # Can be called only set after fit_on_texts was called.
    def inverted_word_index(self):
        if self.cached_inverted_word_index is not None:
            return self.cached_inverted_word_index

        word_index = self.word_index()

        inverted_word_index = {}
        for word, index in word_index.items():
            assert(isinstance(word, str))
            assert(isinstance(index, int))
            assert(index >= 1)
            assert(index not in inverted_word_index)
            inverted_word_index[index] = word
        assert(len(inverted_word_index) == len(word_index))

        self.cached_inverted_word_index = inverted_word_index
        return inverted_word_index
