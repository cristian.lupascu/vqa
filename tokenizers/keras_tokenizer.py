from keras.preprocessing.text import Tokenizer as KerasTok
from tokenizers.tokenizer import Tokenizer


class KerasTokenizer(Tokenizer):

    def __init__(self):
        self.tokenizer = KerasTok(
                num_words=None,
                filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~',
                lower=False,
                split=' ',
                char_level=False,
                oov_token='UNK',
                document_count=0
        )
        super(KerasTokenizer, self).__init__()

    def tokenize_as_str(self, text):
        assert(isinstance(text, str))
        return text

    # This is a NO-OP in this case.
    def announce_batch_tokenization(self, texts):
        pass
