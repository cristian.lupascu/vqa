import tensorflow as tf
from keras import backend as K
from keras.engine import Layer


class GELU(Layer):
    """Gaussian Error Linear Unit.

    This is a smoother version of the RELU.
    Original paper: https://arxiv.org/abs/1606.08415

    The GELU nonlinearity is the expected transformation of a stochastic
    regularizer which randomly applies the identity or zero map to a neuron's
    input. The GELU nonlinearity weights inputs by their magnitude, rather
    than gates inputs by their sign as in ReLUs.

    Args:
        input: Tensor to perform activation.

    Returns:
        The input with the GELU activation applied.
    """

    def __init__(self, **kwargs):
        if K.backend() != 'tensorflow':
            raise RuntimeError('GELU is only available with '
                               'the TensorFlow backend.')
        super(GELU, self).__init__(**kwargs)

    def call(self, inputs):
        cdf = 0.5 * (1.0 + tf.erf(inputs / tf.sqrt(2.0)))
        return inputs * cdf

    def compute_output_shape(self, input_shape):
        return input_shape


if __name__ == "__main__":  # pragma: no cover
    # Plot the activation.

    import matplotlib.pyplot as plt
    import numpy as np

    from keras.layers import Input
    from keras.models import Model

    in_val = Input(shape=(1,), dtype=tf.float32)
    output = GELU()(in_val)
    model = Model(inputs=[in_val], outputs=[output])

    model.compile(loss='categorical_crossentropy', optimizer='adam')
    model.summary()

    x = np.arange(-4.0, 4.0, 0.001)
    y = np.squeeze(model.predict(x, batch_size=256))
    assert(x.shape == y.shape)

    plt.plot(x, y)

    plt.xlabel('x')
    plt.ylabel('GELU(x)')
    plt.title('GELU activation')
    plt.grid(True)
    plt.show()
