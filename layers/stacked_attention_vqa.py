import tensorflow as tf

from keras import backend as K
from keras.engine import Layer


# Paper: https://arxiv.org/abs/1511.02274
class StackedAttentionVQA(Layer):

    # @attention_size is the variable @k from the paper.
    def __init__(self, attention_size, batch_size,
                 return_attention_weights=False, **kwargs):
        if K.backend() != 'tensorflow':
            raise RuntimeError('StackedAttentionVQA is only available with '
                               'the TensorFlow backend.')
        assert(isinstance(attention_size, int))
        assert(isinstance(batch_size, int))
        assert(attention_size >= 1)
        assert(batch_size >= 1)

        self.attention_size = attention_size
        self.batch_size = batch_size
        self.return_attention_weights = return_attention_weights
        self.input_image_dim = None
        self.d = None

        super(StackedAttentionVQA, self).__init__(**kwargs)

    # The model receives two inputs:
    #   * the image feature map (w, h, feature_size)
    #   * the question encoded into a vector (d, ).
    # @input_shape example: [(None, 11, 11, 256), (None, 50)]
    def build(self, input_shape):
        assert(len(input_shape) == 2)
        assert(len(input_shape[0]) == 4)
        assert(len(input_shape[1]) == 2)

        # We are trying to stick with the names from the paper in terms
        # of tensors' dimenensions.
        self.input_image_dim = input_shape[0][1:]  # Ignore batch size.
        self.d = input_shape[1][1]

        self.image_embed_w = self.add_weight(
                        shape=(self.input_image_dim[2], self.d),
                        name='image_embed_w',
                        initializer='glorot_uniform',
                        trainable=True
        )
        self.image_embed_b = self.add_weight(
                        shape=(self.d,),
                        name='image_embed_b',
                        initializer='zeros',
                        trainable=True
        )

        self.question_att_w = self.add_weight(
                        shape=(self.d, self.attention_size),
                        name='question_att_w',
                        initializer='glorot_uniform',
                        trainable=True
        )
        self.question_att_b = self.add_weight(
                        shape=(self.attention_size,),
                        name='question_att_b',
                        initializer='zeros',
                        trainable=True
        )

        self.image_att_w = self.add_weight(
                        shape=(self.d, self.attention_size),
                        name='image_att_w',
                        initializer='glorot_uniform',
                        trainable=True
        )
        self.image_att_b = self.add_weight(
                        shape=(self.attention_size,),
                        name='image_att_b',
                        initializer='zeros',
                        trainable=True
        )

        self.prob_att_w = self.add_weight(
                        shape=(self.attention_size, 1),
                        name='prob_att_w',
                        initializer='glorot_uniform',
                        trainable=True
        )
        self.prob_att_b = self.add_weight(
                        shape=(1,),
                        name='prob_att_b',
                        initializer='zeros',
                        trainable=True
        )

        super(StackedAttentionVQA, self).build(input_shape)

    def call(self, inputs, mask=None):
        assert(isinstance(inputs, list))
        assert(len(inputs) == 2)

        feature_map = inputs[0]  # (batch, w, h, feature_size)
        question = inputs[1]  # (batch, emb_dim)
        assert(feature_map.shape[1:] == self.input_image_dim)

        # Stage 1: "For modeling convenience, we use a single layer perceptron
        # to transform each feature vector to a new vector that has the same
        # dimension as the question vector. vI = tanh(WI fI + bI ).
        # self.d is the dimension of the hidden state of the LSTM that
        # the question has been encoded.
        image = tf.reshape(feature_map, [-1, self.input_image_dim[2]])
        # image.shape = (batch_size * w * h, image_dim[2])
        image = tf.nn.xw_plus_b(image, self.image_embed_w, self.image_embed_b)
        # image.shape = (batch_size * w * h, d)
        image = tf.tanh(image)

        # Stage 2: Copy question vector to be able to perform operations.
        vq = tf.expand_dims(question, 1)  # (batch, 1, d)
        vq = tf.tile(
                vq,
                tf.constant([1, self.input_image_dim[0] * self.input_image_dim[1], 1])  # noqa: E501
        )  # (batch, m, d)  m = number of blocks in the image (ex. 11 x 11)
        vq = tf.reshape(vq, [-1, self.d])  # (batch * m, d)
        vq = tf.nn.xw_plus_b(vq, self.question_att_w, self.question_att_b)
        # vq.shape = (batch * m, k), k = self.attention_size

        # Stage 3: Perform image attention and combine with question.
        vi = tf.nn.xw_plus_b(image, self.image_att_w, self.image_att_b)
        # vi.shape= (batch * m, k)
        ha = tf.tanh(vq + vi)  # (batch * m, k)

        # Stage 4: Compute attention scores for image blocks.
        prob = tf.nn.xw_plus_b(ha, self.prob_att_w, self.prob_att_b)
        # prob.shape = (batch * m, 1)
        prob = tf.reshape(prob, [self.batch_size, self.input_image_dim[0] * self.input_image_dim[1]])  # noqa: E501
        # prob.shappe = (batch, m)
        prob = tf.nn.softmax(prob)

        if self.return_attention_weights:
            return tf.reshape(prob, [self.batch_size, self.input_image_dim[0], self.input_image_dim[1]])  # noqa: E501

        # Stage 5: Combine blocks using attention.
        blocks = []
        image = tf.reshape(image, [self.batch_size, self.input_image_dim[0] * self.input_image_dim[1], self.d])  # noqa: E501
        # image.shape = (batch, m, d)
        for b in range(self.batch_size):
            # tf.expand_dims(prob[b, :], 0).shape == (1, w * h)
            # image[b, :, :].shape = (w * h, d)
            blocks.append(tf.matmul(
                tf.expand_dims(prob[b, :], 0),
                image[b, :, :])
            )  # (1, d)

        blocks = tf.stack(blocks)  # (batch, 1, d)
        blocks = tf.reduce_sum(blocks, 1)  # (batch, d)

        attention = tf.add(blocks, question)

        return attention

    def compute_output_shape(self, input_shape):
        assert(len(input_shape) == 2)
        if self.return_attention_weights:
            return (input_shape[0][0], self.input_image_dim[0], self.input_image_dim[1])  # noqa: E501
        return (input_shape[0][0], input_shape[1][1])
